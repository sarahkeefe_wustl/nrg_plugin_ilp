package org.nrg.xnat.ilp;

import org.nrg.framework.annotations.XnatPlugin;
import org.springframework.context.annotation.ComponentScan;

@XnatPlugin(value = "nrg_plugin_ilp", name = "XNAT 1.7 ILP Plugin", description = "This is the XNAT 1.7 ILP Plugin.")
@ComponentScan({"org.nrg.xnat.workflow.listeners"})
public class IlpPlugin {
}